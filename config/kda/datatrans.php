<?php 


return [
    'merchant_id' => env('DATATRANS_MERCHANT_ID', ''),
    'password' =>  env('DATATRANS_PASSWORD', ''),
    'endpoint' => 'https://api.sandbox.datatrans.com/v1',
    'redirect' => [
        'success'=> 'http://localhost/pay/success',
        
    ]

];