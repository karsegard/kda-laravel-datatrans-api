<?php

namespace KDA\Datatrans;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use KDA\Datatrans\API;

class Provider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConfigs();

        if ($this->app->runningInConsole()) {
            $this->registerConsoleCommands();
        }
        //dump('hey',API::class);
        
        $this->app->singleton(API::class, function ($app) {
        //    dd(config('kda.datatrans.endpoint'));
        //dump('hey');
            return new API(config('kda.datatrans.endpoint'));
        });
        $this->registerPublishableResources();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router, Dispatcher $event)
    {


    }

    private function registerConsoleCommands()
    {

     //   $this->commands(Commands\ImportEventStatus::class);
    }


    /**
     * Load helpers.
     */
    protected function loadHelpers()
    {
        foreach (glob(__DIR__ . '/../Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }


    public function registerConfigs()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__, 1) . '/config/kda/datatrans.php',
            'kda.datatrans'
        );
    }


    /**
     * Register the publishable files.
     */
    private function registerPublishableResources()
    {
        $configPath = dirname(__DIR__, 1) . '/config/kda';

        $publishable = [

            'config' => [
                "{$configPath}/" => config_path('kda')
            ],

        ];
        foreach ($publishable as $group => $paths) {
            $this->publishes($paths, $group);
        }
    }
}
