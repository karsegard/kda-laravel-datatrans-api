<?php

namespace KDA\Datatrans;

use Illuminate\Support\Facades\Http;

use KDA\Rest\Rest;
use KDA\Rest\Collections\Response as RestResponse;
use KDA\Datatrans\Collections\DatatransResponse ;
use KDA\Datatrans\Collections\Transaction ;
use Illuminate\Http\Request;
class API extends Rest
{
    


    public function __construct($baseUrl)
    {
        parent::__construct($baseUrl);
        $auth = config('kda.datatrans.merchant_id') . ":" . config("kda.datatrans.password");
        $this->headers['Authorization'] = "Basic " . base64_encode($auth);
    }

    //initiate a payment
    public function init($amount,$ref,$alias=true){
        $data = [
            'currency'=> 'CHF',
            'amount'=>$amount,
            'refno'=>$ref,
            'option'=> ['createAlias'=>$alias],
            'redirect'=> [
                'successUrl'=> config('kda.datatrans.redirect.success')
            ]
        ];
        return new RestResponse($this->post('/transactions',$data),DatatransResponse::class);
    }

    //retrieve a transaction
    public function status ($txid){
        return new RestResponse($this->get('/transactions/'.$txid),Transaction::class);
    }


    //cancel

    //settle

    //refund

    //authorize

    //verify an alias
    public function verify ($currency,$ref,$authRequest) {
        $data = [
            'currency'=> $currency,
            'refno'=>$ref,
            ...$authRequest
        ];
        $response = $this->post('/transactions/validate',$data);
        return new RestResponse($response,Transaction::class);
    }

    public function verifyCard ($currency,$ref,$token,$expiryMonth,$expiryYear,$_3D=null){
        $payload =  ['card'=> [
            "alias"=>$token,
            "expiryMonth"=> $expiryMonth,
            "expiryYear"=> $expiryYear,
            "3D"=>$_3D
        ]];

        return verify($currency,$ref,$payload);
    }

  
}
