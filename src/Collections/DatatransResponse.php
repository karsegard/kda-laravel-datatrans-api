<?php


namespace KDA\Datatrans\Collections;


class Error extends \KDA\Rest\Collections\NestedObject{
   

}


class Transaction extends \KDA\Rest\Collections\NestedObject{
}

class TransactionFromId extends \KDA\Rest\Collections\NestedObject{
    
    public function __construct ($data){
        
        $this->id = $data;
    }

}



class DatatransResponse extends \KDA\Rest\Collections\NestedObject{

    static $classes = [
        'error' => Error::class,
        'transactionId' => TransactionFromId::class,
       /* 'operations' => Operation::class,
        'customer' => Customer::class*/
    ];

    static public function getNewKeyForKey($key){
        if($key ==='transactionId'){
            return 'transaction';
        }
    }

    static public function getAccessorsKeys(){
        return ['error','transactionId'];
    }

    static public function getClassByKey($key){
        return static::$classes[$key];
    }

    static public function getAccessorsIsArray($key){
       // return $key=='customer'? false:true;
       return false;
    }

}
