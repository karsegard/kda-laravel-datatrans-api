<?php


namespace KDA\Datatrans\Collections;




class Transaction extends \KDA\Rest\Collections\NestedObject{
    static $classes = [
         'history' => TransactionHistoryItem::class,
         'card' => Card::class,
         'PFC' => Card::class,
     ];
 
     static public function getNewKeyForKey($key){
        
     }
 
     static public function getAccessorsKeys(){
         return ['history','card','PFC'];
     }
 
     static public function getClassByKey($key){
         return static::$classes[$key];
     }
 
     static public function getAccessorsIsArray($key){
        return $key=='history'? true:false;
     }
}