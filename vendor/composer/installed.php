<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'a7e0623f4259f762f4886f077b21fe36e67ae041',
        'name' => 'fdt2k/kda-laravel-datatrans-api',
        'dev' => true,
    ),
    'versions' => array(
        'fdt2k/kda-laravel-datatrans-api' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'a7e0623f4259f762f4886f077b21fe36e67ae041',
            'dev_requirement' => false,
        ),
        'fdt2k/kda-laravel-rest' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../fdt2k/kda-laravel-rest',
            'aliases' => array(),
            'reference' => 'c566b3bdbf43c6583aeac424c7a7e25e350c5b80',
            'dev_requirement' => false,
        ),
    ),
);
