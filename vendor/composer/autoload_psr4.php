<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'KDA\\Rest\\' => array($vendorDir . '/fdt2k/kda-laravel-rest/src'),
    'KDA\\Datatrans\\' => array($baseDir . '/src'),
);
